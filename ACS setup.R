
source("C:/Bmore City/Bmore code/R_utilities.R")

required.packages(c("dplyr",
                    "rgdal",
                    "reshape2"))
in.csv<-"C:/Bmore City/ACS/raw_panel.csv"
tracts.dsn<-"C:/Bmore City/Open data/Shapefiles/2010 Census Tracts"
tracts.layer<-"balt_ct_2010"

###

acs<-read.csv(in.csv,
              stringsAsFactors = F)

tracts<-readOGR(tracts.dsn,
        tracts.layer)

id<-c("GEO.id","GEO.id2","GEO.display.label","year")
race.vars<-grep("B02001.HD01",names(acs),value=T)
age.vars<-grep("B01001.HD01",names(acs),value=T)
edu.vars<-grep("S1501.HC01_EST",names(acs),value=T) %>%
  grep("EST",.,value=T)

Race<-select(acs,
             id,
             race.vars)
race.share<-function(var){var<-var/Race$B02001.HD01_VD01}
Race.share<-Race %>%
  mutate_at(race.vars,race.share)
Age<-select(acs,
             id,
             age.vars)
age.share<-function(var){var<-var/Age$B01001.HD01_VD01}
Age.share<-Age %>%
  mutate_at(age.vars,age.share)
Edu<-select(acs,
             id,
             edu.vars)
edu.share<-function(var){var<-var/Edu$S1501.HC01_EST_VC01}

Edu.share<-Edu %>%
  select(id,S1501.HC01_EST_VC05,S1501.HC01_EST_VC01) %>%
  mutate_at(vars(S1501.HC01_EST_VC05,S1501.HC01_EST_VC01),as.numeric) %>%
  mutate_at(vars(S1501.HC01_EST_VC05),edu.share) 

CPI<-read.csv("C:/Bmore City/CPI_2008-2018.csv")
CPI<-melt(CPI,id="Year") %>%
  group_by(year=Year) %>%
  summarise(CPI=mean(value,na.rm=T))
CPI$CPI<- 252.185/CPI$CPI

acs<-left_join(acs,CPI) %>%
  mutate(DP03.HC01_VC85_adj=as.numeric(DP03.HC01_VC85)*CPI)
# str(acs$DP03.HC01_VC85_adj)


real.diff.6<-function(id,v){
  
  diff.var<-ifelse(duplicated(id),v-lag(v,n=6),NA)
  
  return(diff.var)
  
}

# str(Gentrification.Indicators)

Gentrification.Indicators<-left_join(Race.share,Edu.share)
Gentrification.Indicators<-left_join(Gentrification.Indicators,select(acs,id,DP03.HC01_VC85_adj)) %>%
  arrange(GEO.id2,
          year)%>%
  mutate(Income.diff=ifelse(!duplicated(GEO.id2,fromLast = T),real.diff.6(GEO.id2,DP03.HC01_VC85_adj),NA),
         College.diff=ifelse(!duplicated(GEO.id2,fromLast = T),real.diff.6(GEO.id2,S1501.HC01_EST_VC05),NA),
         Race.diff=ifelse(!duplicated(GEO.id2,fromLast = T),real.diff.6(GEO.id2,B02001.HD01_VD02),NA)
         )

write.csv(Gentrification.Indicators,"C:/Bmore City/ACS/clean_panel.csv")
